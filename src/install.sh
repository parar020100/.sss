#! /bin/bash

[[ $SSS_DIR ]] || SSS_DIR=$(readlink -e $(dirname $(readlink -e "$0"))/..);

if [[ -f $SSS_DIR/sss.sh ]]
then
	echo "[ERROR] sss.sh already exists";
	exit 1;
fi
echo "export SSS_DIR=$SSS_DIR" >$SSS_DIR/sss.sh;

include_string="[[ -f $SSS_DIR/sss.sh ]] && . $SSS_DIR/sss.sh";
grep -Fqsx "$include_string" $HOME/.bashrc;
[[ $? = 0 ]] || echo -e "\n$include_string" >>$HOME/.bashrc;

[[ $1 = "-s" ]] || echo "Installing for bash by default"
[[ $1 = "-s" ]] || echo "To install at another shell, add the following line to its rc file:"
[[ $1 = "-s" ]] || echo -e "\n    $include_string\n"

echo ". $SSS_DIR/main.sh" >>$SSS_DIR/sss.sh;
unset include_string

echo "SSS (synced shell settings) installation complete.";
[[ $1 = "-s" ]] || echo "Don't forget to reload shell";
