#! /bin/bash

PATH_add()
{
	newdir=$(realpath $@)
	echo $PATH | grep -Fqsv "$newdir"
	[[ $? = 0 ]] && export PATH=$newdir:$PATH
}
