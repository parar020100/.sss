SSS_MODULES=();

sss_add_module()
{
	dir=$SSS_DIR/modules/$1
	if [[ -d $dir ]]
	then
		for file in $dir/*
		do
			[[ -f $file ]] && sss_add_module $file
		done
		return 0;
	fi

	file=$SSS_DIR/modules/$1.sh;
	[ -f $file ] || file=$1;
	[ -f $file ] || { echo "Module file $file does not exist"; return 1; }

	grep -Fqsx "#SSS_DISABLE" $file
	[ $? = 0 ] || SSS_MODULES=(${SSS_MODULES[@]} $file);

	unset file dir
}
