#SSS_EDITOR="code --wait";
SSS_EDITOR=vim
#SSS_SHELL=$0;

sss_edit_module() #This is broken
{
	exit 1 #This is broken
	[[ ! $1 ]] && { sss_edit_module basic; return $?; };
	[[ $1 = "main" ]] && { sss_edit_module ../main; return $?; };
	[[ $1 = "tmp" ]] && { sss_edit_module local/tmp; return $?; };

	filename=$SSS_DIR/modules/$1.sh;
	changetime="NULL";
	[[ -f $filename ]] && changetime=$(stat -c "%Y" $filename);

	$SSS_EDITOR $filename

	if [[ ( ! -f $filename && $changetime = "NULL") ||
		  ( -f $filename && $changetime != $(stat -c "%Y" $filename)) ]]
	then
		$SSS_SHELL;
	fi
	unset changetime filename;
}

#alias sss='sss_edit_module';
alias sss_dir='cd $SSS_DIR'
alias sss_code='code $SSS_DIR'