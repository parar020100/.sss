SSS_SRC_REMOTE="origin"
SSS_USR_REMOTE="origin"

SSS_SRC_BRANCH="master"
SSS_USR_BRANCH="custom"
SSS_TMPL_BRANCH="template"

complete -W "-n --no-save -h --help" sss_sync

sss_sync()
{
	if [[ $1 = "-h" || $1 = "--help" ]]
	then
		echo "sss_sync [ --no-save ]";
		return 0;
	fi

	echo "Creating a backup of $SSS_DIR"
	[ -d $SSS_DIR.old ] && { echo "Backup already exists; aborting"; return 1; }
	cp -r $SSS_DIR $SSS_DIR.old;

	set -e;

	prefix="\n[SSS SYNC TOOL]"

	echo -e "$prefix Stashing all changes"
	cd $SSS_DIR;
	echo -e "$prefix Stashing existing changes"
	git stash --all;
	[[ $? = 0 ]] || { return 1; };

	echo -e "$prefix Pulling $SSS_SRC_BRANCH from remote $SSS_SRC_REMOTE"
	git checkout $SSS_SRC_BRANCH;
	echo
	git pull --rebase $SSS_SRC_REMOTE $SSS_SRC_BRANCH;

	echo -e "$prefix Pulling $SSS_TMPL_BRANCH from remote $SSS_SRC_REMOTE"
	git checkout $SSS_TMPL_BRANCH;
	echo
	git pull --rebase $SSS_SRC_REMOTE $SSS_TMPL_BRANCH;

	echo -e "$prefix Rebasing $SSS_TMPL_BRANCH onto branch $SSS_SRC_BRANCH"
	git rebase $SSS_SRC_BRANCH $SSS_TMPL_BRANCH;
	[[ $? = 0 ]] || { git rebase --abort; return 1; };

	echo -e "$prefix Pulling $SSS_USR_BRANCH from remote $SSS_USR_REMOTE"
	git checkout $SSS_USR_BRANCH;
	echo
	git pull --rebase $SSS_USR_REMOTE $SSS_USR_BRANCH;

	echo -e "$prefix Rebasing $SSS_USR_BRANCH onto branch $SSS_TMPL_BRANCH"
	git rebase $SSS_TMPL_BRANCH $SSS_USR_BRANCH;
	[[ $? = 0 ]] || { git rebase --abort; return 1; };

	echo -e "$prefix Pushing branches $SSS_USR_BRANCH, $SSS_TMPL_BRANCH and $SSS_SRC_BRANCH to remote $SSS_USR_REMOTE"
	git push -f $SSS_USR_REMOTE $SSS_USR_BRANCH $SSS_TMPL_BRANCH $SSS_SRC_BRANCH

	echo -e "$prefix Restoring stashed changes"
	git stash pop;

	echo -e "$prefix Reinstalling SSS"
	make uninstall quiet-install

	if [[ $1 != "--no-save" && $1 != "-n" && $(git diff 2>/dev/null) != "" ]]
	then
		echo "Last save: $(date +%Y-%m-%d\ %H:%M:%S)" \
			>$SSS_DIR/modules/.last_save.txt
		echo -e "$prefix Committing new changes"
		git add modules/* modules/.* main.sh data
		echo
		git commit -m "Automatic save $(date +%Y-%m-%d\ %H:%M:%S)"

		echo -e "$prefix Pushing branches $SSS_USR_BRANCH, $SSS_TMPL_BRANCH and $SSS_SRC_BRANCH to remote $SSS_USR_REMOTE"
		git push $SSS_USR_REMOTE $SSS_USR_BRANCH $SSS_TMPL_BRANCH $SSS_SRC_BRANCH

	fi

	echo -e "$prefix Git status:"
	git status;
	cd $OLDPWD;

	set +e

	echo -e "$prefix All ok; Deleting the backup"
	rm -rf $SSS_DIR.old

	echo "Don't forget to restart shell!"

}

sss_delete_backup()
{
	rm -rf $SSS_DIR.old
}

sss_restore_backup()
{
	if [[ ! -d $SSS_DIR || ! -d $SSS_DIR.old ]]
	then
		echo "Missing $SSS_DIR or $SSS_DIR.old"
		return 1
	fi
	rm -rf $SSS_DIR; mv $SSS_DIR.old $SSS_DIR
}