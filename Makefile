all: install
reinstall: uninstall install
quiet-install:
	./src/install.sh -s
install:
	./src/install.sh
uninstall:
	./src/restore.sh