. $SSS_DIR/src/modules.sh
. $SSS_DIR/src/sync.sh
. $SSS_DIR/src/edit.sh
. $SSS_DIR/src/path.sh

# include custom modules here

sss_add_module local; # all local modules

for module in ${SSS_MODULES[@]}
do
	. $module;
done

# custom executables

PATH_add "$SSS_DIR/modules/bin"
PATH_add "$SSS_DIR/modules/local/bin"

unset module file SSS_MODULES;
